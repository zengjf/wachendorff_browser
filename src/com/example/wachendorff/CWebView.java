package com.example.wachendorff;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;

public class CWebView extends WebView {
	
	String TAG = "CWebView";

	public CWebView(Context context, AttributeSet attrs) {
		super(context, attrs);
		// TODO Auto-generated constructor stub
	}

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {

        // setMeasuredDimension(820, 500);
    	// because wachendorff设备网站中，有将右下方边距减少20，导致浏览器出现白条，这是避免出现这个问题的方法
    	super.onMeasure(widthMeasureSpec + 22, heightMeasureSpec + 22);
    	
    	Log.e(TAG, "znegjf " + widthMeasureSpec + ", " + heightMeasureSpec);
    }	
}
