package com.example.wachendorff;

import android.app.Activity;


import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextClock;

import android.os.Build;
import android.os.Handler;

public class DateTimeActivity extends Activity{

	private ImageView mBackBtn;
//	private TextClock mTc;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setupMainWindowDisplayMode();

		setContentView(R.layout.ui_date);
//		mTc = (TextClock) findViewById(R.id.tc);
		mBackBtn = (ImageView) findViewById(R.id.back_btn);
//		mTc.setFormat24Hour("dd.MM.yyyy hh:mm:ss");
		mBackBtn.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				finish();
			}
		});
	}
	
	private void setupMainWindowDisplayMode() {
	    View decorView = setSystemUiVisilityMode();
	    decorView.setOnSystemUiVisibilityChangeListener(new View.OnSystemUiVisibilityChangeListener() {
	      @Override
	      public void onSystemUiVisibilityChange(int visibility) {
	        setSystemUiVisilityMode(); // Needed to avoid exiting immersive_sticky when keyboard is displayed
	      }
	    });
	  }
	
	  private View setSystemUiVisilityMode() {
//		if (Build.VERSION.SDK_INT > 11 && Build.VERSION.SDK_INT < 19) {
//			View v = getWindow().getDecorView();
//			v.setSystemUiVisibility(v.GONE);
//			return v;
//		} else if (Build.VERSION.SDK_INT >= 19) {
			View decorView = getWindow().getDecorView();
			int options;
			options =
	        View.SYSTEM_UI_FLAG_LAYOUT_STABLE
	        	| View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
	        	| View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
	        	| View.SYSTEM_UI_FLAG_HIDE_NAVIGATION // hide nav bar
	        	| View.SYSTEM_UI_FLAG_FULLSCREEN // hide status bar
	        	| View.SYSTEM_UI_FLAG_LOW_PROFILE
	        	| View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY;

			decorView.setSystemUiVisibility(options);
			return decorView;
//		}
	  }

	@Override
	public void onWindowFocusChanged(boolean hasFocus) {
		super.onWindowFocusChanged(hasFocus);
		setSystemUiVisilityMode();
	}
}
