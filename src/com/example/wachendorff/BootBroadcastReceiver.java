package com.example.wachendorff;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import android.widget.Toast;

public class BootBroadcastReceiver extends BroadcastReceiver {
//	private final String ACTION_BOOT = "android.intent.action.BOOT_COMPLETED";

    @Override
    public void onReceive(Context context, Intent intent) {
//    	if(ACTION_BOOT.equals(intent.getAction())) {
//    		Toast.makeText(context, "boot complete", Toast.LENGTH_SHORT).show();
    		Intent i = new Intent(context,MainActivity.class);
    		i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
    		context.startActivity(i);
//    	}
    }
}
