package com.example.wachendorff;

import com.example.wachendorff.utils.SPUtils;

import android.util.Log;

import android.app.Activity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnKeyListener;
import android.webkit.MimeTypeMap;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;

import android.os.Build;
import android.os.Handler;

public class URLActivity extends Activity implements OnClickListener, OnCheckedChangeListener{

	private EditText mUrlEdt;
	private ImageView mBackBtn;
	private RadioGroup mSecondGroup;
	private RadioButton mTwoBtn;
	private RadioButton mThreeBtn;
	private RadioButton mFiveBtn;
	private RadioButton mTenBtn;
	private RadioButton mFifteenBtn;
	private RadioButton mThirtyBtn;
	private RadioButton mSixtyBtn;
	
	private String mUrlStr;
	private int mTime;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		SPUtils.setApplication(getApplication());
		SPUtils.setSPFileName("url_config");
		
		setupMainWindowDisplayMode();

		setContentView(R.layout.ui_starturl);
		initView();
		initData();
		initListener();
	}

	private void setupMainWindowDisplayMode() {
	    View decorView = setSystemUiVisilityMode();
	    decorView.setOnSystemUiVisibilityChangeListener(new View.OnSystemUiVisibilityChangeListener() {
	      @Override
	      public void onSystemUiVisibilityChange(int visibility) {
	    	  Log.e("focus change", "-------");
	        setSystemUiVisilityMode(); // Needed to avoid exiting immersive_sticky when keyboard is displayed
	      }
	    });
	  }

	  private View setSystemUiVisilityMode() {
		  /*
		if (Build.VERSION.SDK_INT > 11 && Build.VERSION.SDK_INT < 19) {
			View v = getWindow().getDecorView();
			v.setSystemUiVisibility(v.GONE);
		} else if (Build.VERSION.SDK_INT >= 19) {
		*/
			View decorView = getWindow().getDecorView();
			int options;
			options =
	        View.SYSTEM_UI_FLAG_LAYOUT_STABLE
	        	| View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
	        	| View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
	        	| View.SYSTEM_UI_FLAG_HIDE_NAVIGATION // hide nav bar
	        	| View.SYSTEM_UI_FLAG_FULLSCREEN // hide status bar
	        	| View.SYSTEM_UI_FLAG_LOW_PROFILE
	        	| View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY;

			decorView.setSystemUiVisibility(options);
			return decorView;
//		}
	  }

	@Override
	public void onWindowFocusChanged(boolean hasFocus) {
		super.onWindowFocusChanged(hasFocus);
		setSystemUiVisilityMode();

	}

	private void initData() {
		mTime = SPUtils.getInt("time",5);
		setRadioGroupSelected(mTime);
		mUrlEdt.setText(SPUtils.getString(getApplication(),"url_config","url","http://192.168.100.100:8080/webvisu.htm"));
		
		mUrlEdt.setOnKeyListener(new OnKeyListener() {
			
			@Override
			public boolean onKey(View v, int keyCode, KeyEvent event) {
				// TODO Auto-generated method stub
				if ((event.getAction() == KeyEvent.ACTION_UP) && (keyCode == KeyEvent.KEYCODE_ENTER)) {
					setSystemUiVisilityMode();
				}

				return false;
			}
		});
	}

	private void setRadioGroupSelected(int second) {
		switch (second) {
		case 2:
			mTwoBtn.setChecked(true);
			break;
		case 3:
			mThreeBtn.setChecked(true);
			break;
		case 5:
			mFiveBtn.setChecked(true);
			break;
		case 10:
			mTenBtn.setChecked(true);
			break;
		case 15:
			mFifteenBtn.setChecked(true);
			break;
		case 30:
			mThirtyBtn.setChecked(true);
			break;
		case 60:
			mSixtyBtn.setChecked(true);
			break;
		default:
			break;
		}
	}

	private void initListener() {
		mBackBtn.setOnClickListener(this);
		mSecondGroup.setOnCheckedChangeListener(this);
	}

	private void initView() {
		mUrlEdt = (EditText) findViewById(R.id.url_edit);
		mSecondGroup = (RadioGroup) findViewById(R.id.second_radiogroup);
		mBackBtn = (ImageView) findViewById(R.id.back_btn);
		mTwoBtn =(RadioButton) findViewById(R.id.two_seconds);
		mThreeBtn =(RadioButton) findViewById(R.id.three_seconds);
		mFiveBtn =(RadioButton) findViewById(R.id.five_seconds);
		mTenBtn =(RadioButton) findViewById(R.id.ten_seconds);
		mFifteenBtn =(RadioButton) findViewById(R.id.Fifteen_seconds);
		mThirtyBtn =(RadioButton) findViewById(R.id.Thirty_seconds);
		mSixtyBtn =(RadioButton) findViewById(R.id.Sixty_seconds);
	}

	@Override
	protected void onPause() {
		mUrlStr = mUrlEdt.getText().toString().trim();
		SPUtils.pushString(getApplication(),"url_config","url",mUrlStr);
		SPUtils.pushInt("time", mTime);
		super.onPause();
	}
	@Override
	public void onCheckedChanged(RadioGroup group, int checkedId) {
		switch (checkedId) {
		case R.id.two_seconds:
			mTime = 2;
			break;
		case R.id.three_seconds:
			mTime = 3;
			break;
		case R.id.five_seconds:
			mTime = 5;
			break;
		case R.id.ten_seconds:
			mTime = 10;
			break;
		case R.id.Fifteen_seconds:
			mTime = 15;
			break;
		case R.id.Thirty_seconds:
			mTime = 30;
			break;
		case R.id.Sixty_seconds:
			mTime = 60;
			break;
		default:
			break;
		}
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.back_btn:
			saveState();
			finish();
			break;

		default:
			break;
		}
	}

	private void saveState() {
		
	}
}
