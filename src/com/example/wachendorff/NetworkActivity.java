package com.example.wachendorff;

import com.example.wachendorff.R;

import com.example.wachendorff.R.layout;

import android.app.Activity;

import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnKeyListener;
import android.widget.ImageView;
import android.widget.TextClock;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import android.util.Log;
import android.os.Handler;

import android.net.ConnectivityManager;

import com.example.wachendorff.ethernet.EthernetConfigDialog;
import com.example.wachendorff.ethernet.EthernetEnabler;
import com.example.wachendorff.ethernet.EthernetDevInfo;
import com.example.wachendorff.ethernet.EthernetManager;
//import com.example.wachendorff.ethernet.EthernetConfigDialog.ConfigHandler;

import android.os.Build;
import android.content.BroadcastReceiver;
import android.net.NetworkInfo;
import android.net.NetworkInfo.State;
import android.content.Context;
import android.content.IntentFilter;
import android.content.Intent;
import android.os.SystemProperties;
import android.os.Handler;

import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

//public class NetworkActivity extends Activity {
public class NetworkActivity extends Activity implements OnClickListener {
	Button applyBtn;
	private ImageView backBtn;
	public EditText ipEdt;
	public EditText netMaskEdt;
	public EditText gatewayEdt;
	public TextView currentIp;
	public TextView currentGateway;
	public TextView currentNetmask;
	 private String TAG = "EthernetMainActivity";

    private EthernetEnabler mEthEnabler;
    private EthernetConfigDialog mEthConfigDialog;
    
    private Handler configHandler = new Handler();
    
    private ConnectivityManager  mConnMgr;

    private final BroadcastReceiver mEthernetReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            if(ConnectivityManager.CONNECTIVITY_ACTION.equals(intent.getAction())){
                NetworkInfo info =
                    intent.getParcelableExtra(ConnectivityManager.EXTRA_NETWORK_INFO);
                if (info != null) {
                    Log.i(TAG,"getState()="+info.getState() + "getType()=" +
                            info.getType());
                    if (info.getType() == ConnectivityManager.TYPE_ETHERNET) {
                    Log.e(TAG,"TYPE_ETHERNET----------------------------------------");
                        if (info.getState() == State.DISCONNECTED)
                            mConnMgr.setGlobalProxy(null);
                            SystemProperties.set("rw.HTTP_PROXY", "");
                        if (info.getState() == State.CONNECTED)
                            mEthEnabler.getManager().initProxy();
                    }
                }
            }

		SharedPreferences sp = getSharedPreferences("ethernet",
                Context.MODE_WORLD_WRITEABLE);
		currentIp.setText(sp.getString("mIpaddr", "0.0.0.0"));
		currentNetmask.setText(sp.getString("mNetMask", "0.0.0.0"));
        currentGateway.setText(sp.getString("mGateway", "0.0.0.0"));
        }
    };
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		/*
		if (Build.VERSION.SDK_INT > 11 && Build.VERSION.SDK_INT < 19) {
			View v = getWindow().getDecorView();
			v.setSystemUiVisibility(v.GONE);
		} else if (Build.VERSION.SDK_INT >= 19) {
		
			View decorView = getWindow().getDecorView();
			int uiOptions = 
					View.SYSTEM_UI_FLAG_FULLSCREEN // hide status bar
                    | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN // make content behind status bar
                    | View.SYSTEM_UI_FLAG_LAYOUT_STABLE  // keep layout stable
                    | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION // hide navigation bar
                    | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION // make content behind navigation bar
                    | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY // immersive mode
				;
			/*
			int uiOptions = View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION 
						| View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN 
						| View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
						| View.SYSTEM_UI_FLAG_FULLSCREEN
						| View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
						| View.SYSTEM_UI_FLAG_LOW_PROFILE;

						*/
		/*
			decorView.setSystemUiVisibility(uiOptions);
		}
		*/
		setupMainWindowDisplayMode();

		setContentView(R.layout.ui_network);
		SharedPreferences sp = getSharedPreferences("ethernet",
                Context.MODE_WORLD_WRITEABLE);


		initView();

		currentIp.setText(sp.getString("mIpaddr", "0.0.0.0"));
		Log.e("ip", sp.getString("mIpaddr", "1.1.1.1"));
		currentNetmask.setText(sp.getString("mNetMask", "0.0.0.0"));
		Log.e("gateway", sp.getString("mGateway", "1.1.1.1"));
        currentGateway.setText(sp.getString("mGateway", "0.0.0.0"));
		Log.e("netmask", sp.getString("mNetMask", "1.1.1.1"));

		initListener();

		mEthEnabler = new EthernetEnabler(this);
		
		mConnMgr = (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
        // 设置广播接受者
        IntentFilter filter = new IntentFilter();
        // 监听网络状态变化
        filter.addAction(ConnectivityManager.CONNECTIVITY_ACTION);
        registerReceiver(mEthernetReceiver, filter);
	}
	
	private void setupMainWindowDisplayMode() {
	    View decorView = setSystemUiVisilityMode();
	    decorView.setOnSystemUiVisibilityChangeListener(new View.OnSystemUiVisibilityChangeListener() {
	      @Override
	      public void onSystemUiVisibilityChange(int visibility) {
	    	  Log.e("focus change", "-------");
	          setSystemUiVisilityMode(); // Needed to avoid exiting immersive_sticky when keyboard is displayed
	      }
	    });
	  }

	  private View setSystemUiVisilityMode() {
		  /*
		if (Build.VERSION.SDK_INT > 11 && Build.VERSION.SDK_INT < 19) {
			View v = getWindow().getDecorView();
			v.setSystemUiVisibility(v.GONE);
		} else if (Build.VERSION.SDK_INT >= 19) {
		*/
			View decorView = getWindow().getDecorView();
			int options;
			options =
	        View.SYSTEM_UI_FLAG_LAYOUT_STABLE
	        	| View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
	        	| View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
	        	| View.SYSTEM_UI_FLAG_HIDE_NAVIGATION // hide nav bar
	        	| View.SYSTEM_UI_FLAG_FULLSCREEN // hide status bar
	        	| View.SYSTEM_UI_FLAG_LOW_PROFILE
	        	| View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY;

			decorView.setSystemUiVisibility(options);
			return decorView;
//		}
	  }
	@Override
	public void onWindowFocusChanged(boolean hasFocus) {
		super.onWindowFocusChanged(hasFocus);
		setSystemUiVisilityMode();
	}

	private void initView() {
		applyBtn = (Button) findViewById(R.id.apply_btn);
		backBtn = (ImageView) findViewById(R.id.back_btn);
		ipEdt = (EditText) findViewById(R.id.nw_ip_setting);
		netMaskEdt = (EditText) findViewById(R.id.nw_mask_setting);
		gatewayEdt = (EditText) findViewById(R.id.nw_gateway_setting);
		currentIp = (TextView)findViewById(R.id.nw_current_ip);
		currentNetmask = (TextView)findViewById(R.id.nw_current_mask);
		currentGateway = (TextView)findViewById(R.id.nw_current_gateway);
		
		gatewayEdt.setOnKeyListener(new OnKeyListener() {
			
			@Override
			public boolean onKey(View v, int keyCode, KeyEvent event) {
				// TODO Auto-generated method stub
				if ((event.getAction() == KeyEvent.ACTION_UP) && (keyCode == KeyEvent.KEYCODE_ENTER)) {
					setSystemUiVisilityMode();
				}

				return false;
			}
		});

	}

	private void initListener() {
		backBtn.setOnClickListener(this);
		applyBtn.setOnClickListener(this);

	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.back_btn:
			finish();
			break;
		case R.id.apply_btn:
			configNetwork();
			gatewayEdt.clearFocus();
			ipEdt.clearFocus();
			netMaskEdt.clearFocus();
			break;
		default:
			break;
		}
	}

	public void configNetwork(){
		mEthConfigDialog = new EthernetConfigDialog(this, mEthEnabler);
		mEthConfigDialog.setTextEdit(ipEdt, netMaskEdt, gatewayEdt);
		mEthConfigDialog.buildDialogContent(this);
		mEthEnabler.setConfigDialog(mEthConfigDialog);
	}
	
	@Override
	protected void onDestroy() {
		super.onDestroy();
		unregisterReceiver(mEthernetReceiver);
	}
}