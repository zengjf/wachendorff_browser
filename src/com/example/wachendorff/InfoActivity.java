package com.example.wachendorff;

import android.app.Activity;

import android.util.Log;
import android.graphics.Point;

import android.os.Bundle;
import android.view.Display;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import android.os.Build;
import android.os.Handler;

public class InfoActivity extends Activity implements OnClickListener {
	private TextView mOs;
	private TextView mDisplay;
	private TextView mVsersion;
	private ImageView mBackBtn;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setupMainWindowDisplayMode();

		setContentView(R.layout.ui_info);
		initView();
		initData();
		initListener();
		// android.os.Build.VERSION.RELEASE
	}
	
	private void setupMainWindowDisplayMode() {
	    View decorView = setSystemUiVisilityMode();
	    decorView.setOnSystemUiVisibilityChangeListener(new View.OnSystemUiVisibilityChangeListener() {
	      @Override
	      public void onSystemUiVisibilityChange(int visibility) {
	    	  Log.e("focus change", "-------");
	        setSystemUiVisilityMode(); // Needed to avoid exiting immersive_sticky when keyboard is displayed
	      }
	    });
	  }

	  private View setSystemUiVisilityMode() {
//		if (Build.VERSION.SDK_INT > 11 && Build.VERSION.SDK_INT < 19) {
//			View v = getWindow().getDecorView();
//			v.setSystemUiVisibility(v.GONE);
//		} else if (Build.VERSION.SDK_INT >= 19) {
			View decorView = getWindow().getDecorView();
			int options;
			options =
	        View.SYSTEM_UI_FLAG_LAYOUT_STABLE
	        	| View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
	        	| View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
	        	| View.SYSTEM_UI_FLAG_HIDE_NAVIGATION // hide nav bar
	        	| View.SYSTEM_UI_FLAG_FULLSCREEN // hide status bar
	        	| View.SYSTEM_UI_FLAG_LOW_PROFILE
	        	| View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY;

			decorView.setSystemUiVisibility(options);
			return decorView;
//		}
	  }

	@Override
	public void onWindowFocusChanged(boolean hasFocus) {
		super.onWindowFocusChanged(hasFocus);

		setSystemUiVisilityMode();

		/*
		if (Build.VERSION.SDK_INT > 11 && Build.VERSION.SDK_INT < 19) {
			View v = getWindow().getDecorView();
			v.setSystemUiVisibility(v.GONE);
		} else if (Build.VERSION.SDK_INT >= 19) {
		
			View decorView = getWindow().getDecorView();
			int uiOptions = 
					View.SYSTEM_UI_FLAG_FULLSCREEN // hide status bar
                    | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN // make content behind status bar
                    | View.SYSTEM_UI_FLAG_LAYOUT_STABLE  // keep layout stable
                    | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION // hide navigation bar
                    | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION // make content behind navigation bar
                    | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY // immersive mode
				;
			decorView.setSystemUiVisibility(uiOptions);
		}
		*/
	}

	private void initListener() {
		mBackBtn.setOnClickListener(this);
	}

	private void initData() {
		mOs.setText("OS-Version: Android " + android.os.Build.VERSION.RELEASE);
		/*
		WindowManager windowManager = getWindowManager();
		Display display = windowManager.getDefaultDisplay();
		int screenWidth = screenWidth = display.getWidth();
		int screenHeight = screenHeight = display.getHeight();
		*/
		Display display = getWindowManager().getDefaultDisplay(); 
	    Point size = new Point(); 
	    display.getRealSize(size); 
	    int screenWidth = size.x; 
	    int screenHeight = size.y;
		
		mDisplay.setText("Display: " + screenWidth + " x " +screenHeight);
	}

	private void initView() {
		mOs = (TextView) findViewById(R.id.info_os);
		mDisplay = (TextView) findViewById(R.id.info_display);
		mVsersion = (TextView) findViewById(R.id.info_version);
		mBackBtn = (ImageView) findViewById(R.id.back_btn);
	}

	@Override
	public void onClick(View v) {
		finish();
	}
}
