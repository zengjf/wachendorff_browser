package com.example.wachendorff;

import android.app.Activity;
import android.app.Dialog;

import android.os.Build;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.webkit.HttpAuthHandler;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import android.annotation.SuppressLint;
import android.content.DialogInterface;

import android.os.Build;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.os.Handler;

@SuppressLint("NewApi")
public class WebActivity extends Activity{
	private WebView mWebView;
	private String mUrlStr;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setupMainWindowDisplayMode();

		setContentView(R.layout.ui_web);

		initView();
		initData();
	}
	
	private void setupMainWindowDisplayMode() {
	    View decorView = setSystemUiVisilityMode();
	    decorView.setOnSystemUiVisibilityChangeListener(new View.OnSystemUiVisibilityChangeListener() {
	      @Override
	      public void onSystemUiVisibilityChange(int visibility) {
	        setSystemUiVisilityMode(); // Needed to avoid exiting immersive_sticky when keyboard is displayed
	      }
	    });
	  }

	  private View setSystemUiVisilityMode() {
		  /*
		if (Build.VERSION.SDK_INT > 11 && Build.VERSION.SDK_INT < 19) {
			View v = getWindow().getDecorView();
			v.setSystemUiVisibility(v.GONE);
		} else if (Build.VERSION.SDK_INT >= 19) {
		*/
			View decorView = getWindow().getDecorView();
			int options;
			options =
	        View.SYSTEM_UI_FLAG_LAYOUT_STABLE
	        	| View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
	        	| View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
	        	| View.SYSTEM_UI_FLAG_HIDE_NAVIGATION // hide nav bar
	        	| View.SYSTEM_UI_FLAG_FULLSCREEN // hide status bar
	        	| View.SYSTEM_UI_FLAG_LOW_PROFILE
	        	| View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY;

			decorView.setSystemUiVisibility(options);
			return decorView;
//		}
	  }

	@Override
	public void onWindowFocusChanged(boolean hasFocus) {
		super.onWindowFocusChanged(hasFocus);

		setSystemUiVisilityMode();

		/*
		if (Build.VERSION.SDK_INT > 11 && Build.VERSION.SDK_INT < 19) {
			View v = getWindow().getDecorView();
			v.setSystemUiVisibility(v.GONE);
		} else if (Build.VERSION.SDK_INT >= 19) {
		
			View decorView = getWindow().getDecorView();
			int uiOptions = 
					View.SYSTEM_UI_FLAG_FULLSCREEN // hide status bar
                    | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN // make content behind status bar
                    | View.SYSTEM_UI_FLAG_LAYOUT_STABLE  // keep layout stable
                    | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION // hide navigation bar
                    | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION // make content behind navigation bar
                    | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY // immersive mode
				;

			decorView.setSystemUiVisibility(uiOptions);
		}
		*/
	}

		

	private void initView() {
		mWebView = (WebView) findViewById(R.id.wv);
	}
    private void initData() {
    	mUrlStr = getIntent().getStringExtra("url");
        WebSettings webSettings = mWebView.getSettings();
        //chrome远程调试使用
        WebView.setWebContentsDebuggingEnabled(true); 
        //设置支持javaScript
        webSettings.setJavaScriptEnabled(true);
        webSettings.setUseWideViewPort(true);   
        webSettings.setLoadWithOverviewMode(true);   
          
        webSettings.setSupportZoom(true);    
        /**
         * 用WebView显示图片，可使用这个参数 设置网页布局类型： 1、LayoutAlgorithm.NARROW_COLUMNS ：
         * 适应内容大小 2、LayoutAlgorithm.SINGLE_COLUMN:适应屏幕，内容将自动缩放
         */
        webSettings.setLayoutAlgorithm(WebSettings.LayoutAlgorithm.SINGLE_COLUMN);    

        mWebView.loadUrl(mUrlStr);
        //自己设置client 防止弹出自带浏览器
        mWebView.setWebViewClient(new AplexWebViewClient());
        

    }
    private class AplexWebViewClient extends WebViewClient{
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return true;
        }
        
        public String usernameString = null;
        public String passwordString = null;
        public EditText username = null;
        public EditText password = null;
        public HttpAuthHandler httpAuthHandler;
        public WebView myView;
        public String myHost;
        public String myRealm;
        @Override
        public void onReceivedHttpAuthRequest(WebView view,
        		HttpAuthHandler handler, String host, String realm) {
        	// TODO Auto-generated method stub
        	// super.onReceivedHttpAuthRequest(view, handler, host, realm);

            this.httpAuthHandler = handler;
            this.myView = view;
            this.myHost = host;
            this.myRealm= realm;

            final Dialog dialog = new Dialog(WebActivity.this, R.style.Dialog_Color);
            // dialog.getWindow().setBackgroundDrawableResource(R.color.dialog_signin_title);
            dialog.setContentView(R.layout.dialog_signin);
            dialog.setTitle("Authentifizierung Erforderlich");
            
            username = (EditText) dialog.findViewById(R.id.username);
            password = (EditText) dialog.findViewById(R.id.password);

            TextView descriptor = (TextView)dialog.findViewById(R.id.descriptor);
            descriptor.setText("für " + mUrlStr + " sind ein Nut-zername und ein Passwort erforderlich. Die Verbindung zu dieser Website ist nicht sicher");

            Button ok = (Button) dialog.findViewById(R.id.ok);
            Button cancel = (Button) dialog.findViewById(R.id.cancel);
            
            ok.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
            	usernameString = username.getText().toString();
            	passwordString = password.getText().toString();
                dialog.dismiss();
				Log.e("WebActivity", usernameString + ", " + passwordString);
				if (passwordString.trim().length() != 0 && usernameString.length() != 0)
					httpAuthHandler.proceed(usernameString, passwordString);
            }
			});

            cancel.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                AplexWebViewClient.super.onReceivedHttpAuthRequest(myView, httpAuthHandler, myHost, myRealm);
            }
			});

            dialog.show();

        	Log.d("WebActivity.java", "onReceivedHttpAuthRequest()");
        }
    }
}
