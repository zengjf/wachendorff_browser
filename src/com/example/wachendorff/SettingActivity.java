package com.example.wachendorff;

//import com.example.wachendorff.ethernet.NetworkActivity;


import android.app.Activity;
import android.util.Log;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import android.os.Build;
import android.os.Handler;

public class SettingActivity extends Activity implements OnClickListener {
	private LinearLayout mStartUrlBtn;
	private LinearLayout mDateAndTime;
	//private RelativeLayout mDisplaySettingsBtn;
	private LinearLayout mNetworkSettingsBtn;
	private LinearLayout mInfoBtn;
	private ImageView mBackBtn;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setupMainWindowDisplayMode();
		setContentView(R.layout.ui_settings);
		initView();
		initListener();
	}
	
	private void setupMainWindowDisplayMode() {
	    View decorView = setSystemUiVisilityMode();
	    decorView.setOnSystemUiVisibilityChangeListener(new View.OnSystemUiVisibilityChangeListener() {
	      @Override
	      public void onSystemUiVisibilityChange(int visibility) {
	    	  Log.e("focus change", "-------");
	        setSystemUiVisilityMode(); // Needed to avoid exiting immersive_sticky when keyboard is displayed
	      }
	    });
	  }

	  private View setSystemUiVisilityMode() {
		  /*
		if (Build.VERSION.SDK_INT > 11 && Build.VERSION.SDK_INT < 19) {
			View v = getWindow().getDecorView();
			v.setSystemUiVisibility(v.GONE);
		} else if (Build.VERSION.SDK_INT >= 19) {
		*/
			View decorView = getWindow().getDecorView();
			int options;
			options =
	        View.SYSTEM_UI_FLAG_LAYOUT_STABLE
	        	| View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
	        	| View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
	        	| View.SYSTEM_UI_FLAG_HIDE_NAVIGATION // hide nav bar
	        	| View.SYSTEM_UI_FLAG_FULLSCREEN // hide status bar
	        	| View.SYSTEM_UI_FLAG_LOW_PROFILE
	        	| View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY;

			decorView.setSystemUiVisibility(options);
			return decorView;
//		}
	  }
	
	@Override
	public void onWindowFocusChanged(boolean hasFocus) {
		super.onWindowFocusChanged(hasFocus);

		setSystemUiVisilityMode();
	}

	private void initView() {
		mStartUrlBtn = (LinearLayout) findViewById(R.id.url_btn);
		mDateAndTime = (LinearLayout) findViewById(R.id.data_time_btn);
		//mDisplaySettingsBtn = (RelativeLayout) findViewById(R.id.display_btn);
		mNetworkSettingsBtn = (LinearLayout) findViewById(R.id.network_btn);
		mInfoBtn = (LinearLayout) findViewById(R.id.info_btn);
		mBackBtn = (ImageView) findViewById(R.id.back_btn);
	}

	private void initListener() {
		mStartUrlBtn.setOnClickListener(this);
		mDateAndTime.setOnClickListener(this);
		//mDisplaySettingsBtn.setOnClickListener(this);
		mNetworkSettingsBtn.setOnClickListener(this);
		mInfoBtn.setOnClickListener(this);
		mBackBtn.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.url_btn:
			toUrlActivity();
			break;

		case R.id.data_time_btn:
			toDateTimeActivity();
			break;
			
		case R.id.network_btn:
			toNetworkActivity();
		
			break;
		case R.id.info_btn:
			toInfoActivity();
			break;
			
		case R.id.back_btn:
			toBack();
		default:
			break;
		}
	}

	

	private void toBack() {
		finish();
	}

	private void toInfoActivity() {
		Intent intent = new Intent(this,InfoActivity.class);
		startActivity(intent);
	}

	private void toNetworkActivity() {
		Intent intent = new Intent(this,NetworkActivity.class);
		startActivity(intent);
	}

	private void toDateTimeActivity() {
Intent intent = new Intent(this,DateTimeActivity.class);
		startActivity(intent);
	}

	private void toUrlActivity() {
		Intent intent = new Intent(this,URLActivity.class);
		startActivity(intent);
	}
}

