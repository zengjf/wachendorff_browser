/*
 * Copyright (C) 2013-2015 Freescale Semiconductor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.example.wachendorff.ethernet;

import com.example.wachendorff.ethernet.EthernetDevInfo;

import com.example.wachendorff.R;
import com.example.wachendorff.R.layout;
import com.example.wachendorff.NetworkActivity;

import android.view.View;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.os.Handler;
import android.widget.LinearLayout;
import android.widget.CompoundButton;
import android.widget.CheckBox;
import android.widget.Toast;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.view.LayoutInflater;


/**
 * Created by B38613 on 13-8-5.
 */

public class EthernetConfigDialog {
//public class EthernetConfigDialog extends AlertDialog implements
//        DialogInterface.OnClickListener, AdapterView.OnItemSelectedListener, View.OnClickListener {
    private final String TAG = "EtherenetSettings";
    private static final boolean localLOGV = true;
    private Context mContext;
    private EthernetEnabler mEthEnabler;
    private View mView;
    
    private RelativeLayout mRootLayout;
    
    private EditText mIpaddr;
    private EditText mMask;
    private EditText mGateway;

    private static String Mode_dhcp = "dhcp";
    private Handler configHandler = new Handler();

    public EthernetConfigDialog(Context context, EthernetEnabler Enabler) {
//        super(context);

        mContext = context;
        mEthEnabler = Enabler;
//        buildDialogContent(context);
        
//        handle_saveconf(context);
    }
    
    public int buildDialogContent(Context context) {
//    	LayoutInflater layoutInflater = LayoutInflater.from(context);
//        View mView = layoutInflater.inflate(R.layout.ui_network, null);
//        View mRootView = layoutInflater.inflate(R.layout.ui_network_layout, null);
//        mRootLayout = layoutInflater.inflate(R.layout.ui_network_layout, null);
//        mLayout.addView(mView);
//        mIpaddr = (EditText) mView.findViewById(R.id.nw_ip_setting);
//        mMask = (EditText) mView.findViewById(R.id.nw_mask_setting);
//        mGateway = (EditText) mView.findViewById(R.id.nw_gateway_setting);
    	Log.e("debug", "ip: " +  mIpaddr.getText().toString());
    	Log.e("debug", "mask: " +  mMask.getText().toString());
    	Log.e("debug", "gateway: " + mGateway.getText().toString());
    	
    	if (mEthEnabler.getManager().isConfigured()) {
            EthernetDevInfo info = mEthEnabler.getManager().getSavedConfig();
    	}

    	handle_saveconf(context);
        return 0;
    }
    public void setTextEdit(EditText ip, EditText mask, EditText gateway) {
    	this.mIpaddr = ip;
    	this.mMask = mask;
    	this.mGateway = gateway;
    }

    public void handle_saveconf(Context context) {
    	Log.e("debug", "ip: " + mIpaddr.getText().toString());
        EthernetDevInfo info = new EthernetDevInfo();
    	Log.e("debug", "dev info");
        info.setIfName("eth0");
        if ((mIpaddr.getText().toString().equals("")) || (mMask.getText().toString().equals("")) ||
            mGateway.getText().toString().equals("")) {
            Toast.makeText(context, "ip/mask/gateway is empty",Toast.LENGTH_SHORT).show();
        info.setIfName("eth0");
            return;
        } else {
            info.setConnectMode(EthernetDevInfo.ETHERNET_CONN_MODE_MANUAL);
            info.setIpAddress(mIpaddr.getText().toString());
            info.setDnsAddr("");
            info.setNetMask(mMask.getText().toString());
            info.setGateway(mGateway.getText().toString());
            info.setProxyAddr(mEthEnabler.getManager().getSharedPreProxyAddress());
            info.setProxyPort(mEthEnabler.getManager().getSharedPreProxyPort());
            info.setProxyExclusionList(mEthEnabler.getManager().getSharedPreProxyExclusionList());
            configHandler.post(new ConfigHandler(info));
        }
    }

    class ConfigHandler implements Runnable {
        EthernetDevInfo info;

        public ConfigHandler(EthernetDevInfo info) {
            this.info = info;
        }

        public void run() {
        	// 更新设置的 数据
            mEthEnabler.getManager().updateDevInfo(info);
            mEthEnabler.setEthEnabled();
        }
    }
/*
    public void onClick(DialogInterface dialog, int which) {
        switch (which) {
            case BUTTON_POSITIVE:
                handle_saveconf();
                break;
            case BUTTON_NEGATIVE:
                dialog.cancel();
                break;
            default:
                Log.e(TAG,"Unknow button");
        }
    }

    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

    }

    public void onNothingSelected(AdapterView<?> parent) {

    }

    public void onClick(View v) {

    }
    */
    /*
    public void updateDevNameList(String[] DevList) {
        if (DevList != null) {
            ArrayAdapter<CharSequence> adapter = new ArrayAdapter<CharSequence>(
                    getContext(), android.R.layout.simple_spinner_item, DevList);
            adapter.setDropDownViewResource(
                    android.R.layout.simple_spinner_dropdown_item);
            mDevList.setAdapter(adapter);
        }
    }
    */
}

