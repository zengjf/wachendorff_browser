package com.example.wachendorff;

import com.example.wachendorff.utils.SPUtils;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.SystemClock;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.util.Log;

import android.os.Build;

public class MainActivity extends Activity implements OnClickListener {
	private LinearLayout urlSetBtn;
	private LinearLayout settingsBtn;
	private TextView mTime;
	private TextView mUrl;

	private int mSecond;
	private String mUrlStr;
	private boolean continueFlag = true;

	private Handler mhandler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			if (msg.what == 1) {
				mTime.setText((Integer) msg.obj + " ...");
			}
			if (msg.what == 2) {
				toWebActivity();
			}
		}

	};

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

//		Toast.makeText(this, "MainActivity", Toast.LENGTH_SHORT).show();

		SPUtils.setApplication(getApplication());
		SPUtils.setSPFileName("url_config");
		setupMainWindowDisplayMode();

		setContentView(R.layout.activity_main);
		initView();
		initListener();
		
	}
	
	private void setupMainWindowDisplayMode() {
	    View decorView = setSystemUiVisilityMode();
	    decorView.setOnSystemUiVisibilityChangeListener(new View.OnSystemUiVisibilityChangeListener() {
	      @Override
	      public void onSystemUiVisibilityChange(int visibility) {
	    	  Log.e("focus change", "-------");
	        setSystemUiVisilityMode(); // Needed to avoid exiting immersive_sticky when keyboard is displayed
	      }
	    });
	  }

	  private View setSystemUiVisilityMode() {
		  /*
		if (Build.VERSION.SDK_INT > 11 && Build.VERSION.SDK_INT < 19) {
			View v = getWindow().getDecorView();
			v.setSystemUiVisibility(v.GONE);
		} else if (Build.VERSION.SDK_INT >= 19) {
		*/
			View decorView = getWindow().getDecorView();
			int options;
			options =
	        View.SYSTEM_UI_FLAG_LAYOUT_STABLE
	        	| View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
	        	| View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
	        	| View.SYSTEM_UI_FLAG_HIDE_NAVIGATION // hide nav bar
	        	| View.SYSTEM_UI_FLAG_FULLSCREEN // hide status bar
	        	| View.SYSTEM_UI_FLAG_LOW_PROFILE
	        	| View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY;

			decorView.setSystemUiVisibility(options);
			return decorView;
//		}
	  }

	@Override
	public void onWindowFocusChanged(boolean hasFocus) {
		super.onWindowFocusChanged(hasFocus);
		setSystemUiVisilityMode();
	}
		

	private void toWebActivity() {
		Intent intent = new Intent(this, WebActivity.class);
		intent.putExtra("url", mUrl.getText().toString().trim());
		startActivity(intent);
	}

	
	@Override
	protected void onResume() {
		intiData();
		continueFlag = true;
		startCountTime();
		super.onResume();
	}
	

	@Override
	protected void onPause() {
		continueFlag = false;
		super.onPause();
	}

	private void startCountTime() {
		new Thread(new Runnable() {
			public void run() {
				for (int i = mSecond; i >= 0; i--) {
					SystemClock.sleep(1000);
					if (!continueFlag) {
						return;
					}
					if (i == 0) {
						Message m = mhandler.obtainMessage();
						m.what = 2;
						m.sendToTarget();
						return;
					}
					Message m = mhandler.obtainMessage();
					m.what = 1;
					m.obj = i;
					m.sendToTarget();
				}
			}
		}).start();
	}

	private void initView() {
		// 对LinearLayout布局设置监听
		urlSetBtn = (LinearLayout) findViewById(R.id.urlset_btn);
		// 对RelativeLayout布局设置监听
		settingsBtn = (LinearLayout) findViewById(R.id.settings_btn);
		// 时间
		mTime = (TextView) findViewById(R.id.tv_time);
		// uir
		mUrl = (TextView) findViewById(R.id.tv_url);
	}

	private void intiData() {
		// 获取“time”的设置值，默认值是5
		mSecond = SPUtils.getInt("time", 5);
		// 获取“URL”
		mUrlStr = SPUtils.getString(getApplication(), "url_config", "url", "http://192.168.100.100:8080/webvisu.htm");
		mTime.setText(mSecond + "");
		mUrl.setText(mUrlStr);
	}

	private void initListener() {
		urlSetBtn.setOnClickListener(this);
		settingsBtn.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		// 相关配置
		case R.id.settings_btn:
			toSettingsActivity();
			break;
		// 设置url
		case R.id.urlset_btn:
			toUrlSetActivity();
			break;

		default:
			break;
		}

	}

	private void toUrlSetActivity() {
		Intent intent = new Intent(this, URLActivity.class);
		startActivity(intent);
	}

	private void toSettingsActivity() {
		Intent intent = new Intent(this, SettingActivity.class);
		startActivity(intent);
	}
}
