package com.example.wachendorff;

import android.app.Activity;
import android.os.Bundle;

import android.view.View;

import android.os.Build;
import android.util.Log;
import android.os.Handler;

public class DisplayActivity extends Activity{

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setupMainWindowDisplayMode();
		
		setContentView(R.layout.ui_display);
	}
	private void setupMainWindowDisplayMode() {
	    View decorView = setSystemUiVisilityMode();
	    decorView.setOnSystemUiVisibilityChangeListener(new View.OnSystemUiVisibilityChangeListener() {
	      @Override
	      public void onSystemUiVisibilityChange(int visibility) {
	    	  Log.e("focus change", "-------");
	        setSystemUiVisilityMode(); // Needed to avoid exiting immersive_sticky when keyboard is displayed
	      }
	    });
	  }

	  private View setSystemUiVisilityMode() {
			View decorView = getWindow().getDecorView();
			int options;
			options =
	        View.SYSTEM_UI_FLAG_LAYOUT_STABLE
	        	| View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
	        	| View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
	        	| View.SYSTEM_UI_FLAG_HIDE_NAVIGATION // hide nav bar
	        	| View.SYSTEM_UI_FLAG_FULLSCREEN // hide status bar
	        	| View.SYSTEM_UI_FLAG_LOW_PROFILE
	        	| View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY;

			decorView.setSystemUiVisibility(options);
			return decorView;
	  }
	  
	@Override
	public void onWindowFocusChanged(boolean hasFocus) {
		super.onWindowFocusChanged(hasFocus);
		setSystemUiVisilityMode();
	}

	@Override
	protected void onResume() {
	    super.onResume();
	    executeDelayed();
	}


	private void executeDelayed() {
	    Handler handler = new Handler();
	    handler.postDelayed(new Runnable() {
	        @Override
	        public void run() {
	            // execute after 500ms
	            hideNavBar();
	        }
	    }, 500);
	}


	private void hideNavBar() {
	    if (Build.VERSION.SDK_INT >= 19) {
	        View v = getWindow().getDecorView();
	        v.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE
	                                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
	                                | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
	                                | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
	                                | View.SYSTEM_UI_FLAG_FULLSCREEN
	                                | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
	    }
	}
}
